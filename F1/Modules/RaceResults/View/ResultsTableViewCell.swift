//
//  ResultsTableViewCell.swift
//  F1
//
//  Created by Michael Yu on 4/21/21.
//

import UIKit

class ResultsTableViewCell: UITableViewCell {
    @IBOutlet private weak var teamView: UIView!
    @IBOutlet private weak var positionLabel: UILabel!
    @IBOutlet private weak var gridLabel: UILabel!
    @IBOutlet private weak var pointsLabel: UILabel!
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var driverLabel: UILabel!
    @IBOutlet private weak var timeView: UIView!
    
    func configure(from configurator: ResultsTableCellViewModelProtocol) {
        teamView.layer.backgroundColor = UIColor(named: configurator.constructorId).cgColor
        positionLabel.text = configurator.position
        gridLabel.text = configurator.grid
        pointsLabel.text = configurator.points
        timeLabel.text = configurator.time
        driverLabel.text = configurator.code
        timeView.layer.cornerRadius = 10
    }
}
