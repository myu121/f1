//
//  RaceResults.swift
//  F1
//
//  Created by Michael Yu on 4/21/21.
//

import Foundation

struct RaceResults: Decodable {
    var mrData: MRRaceResults
    
    enum CodingKeys: String, CodingKey {
        case mrData = "MRData"
    }
    
//
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//
//        mrData = try container.decode(MRRaceResults.self, forKey: .mrData)
//    }
    
}

struct MRRaceResults: Decodable {
    var xmlns: String
    var series: String
    var url: String
    var limit: String
    var offset: String
    var total: String
    var raceTable: RaceTable
    
    enum CodingKeys: String, CodingKey {
        case xmlns, series, url, limit, offset, total
        case raceTable = "RaceTable"
    }
    
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//
//        xmlns = try container.decode(String.self, forKey: .xmlns)
//        series = try container.decode(String.self, forKey: .series)
//        url = try container.decode(String.self, forKey: .url)
//        limit = try container.decode(String.self, forKey: .limit)
//        offset = try container.decode(String.self, forKey: .offset)
//        total = try container.decode(String.self, forKey: .total)
//        raceTable = try container.decode(RaceTable.self, forKey: .raceTable)
//    }
}

struct Results: Decodable {
    var number: String
    var position: String
    var positionText: String
    var points: String
    var driver: Driver
    var constructor: Constructor
    var grid: String
    var laps: String
    var status: String
    var time: Time?
    var fastestLap: FastestLap?
    
    enum CodingKeys: String, CodingKey {
        case number, position, positionText, points, grid, laps, status
        case driver = "Driver"
        case constructor = "Constructor"
        case time = "Time"
        case fastestLap = "FastestLap"
    }
    
//    init(from decoder: Decoder) throws {
//        let resultContainer = try decoder.container(keyedBy: CodingKeys.self)
//
//        number = try resultContainer.decode(String.self, forKey: .number)
//        position = try resultContainer.decode(String.self, forKey: .position)
//        positionText = try resultContainer.decode(String.self, forKey: .positionText)
//        points = try resultContainer.decode(String.self, forKey: .points)
//        grid = try resultContainer.decode(String.self, forKey: .grid)
//        laps = try resultContainer.decode(String.self, forKey: .laps)
//        status = try resultContainer.decode(String.self, forKey: .status)
//        driver = try resultContainer.decode(Driver.self, forKey: .driver)
//        constructor = try resultContainer.decode(Constructor.self, forKey: .constructor)
//        time = try resultContainer.decode(Time.self, forKey: .time)
//        fastestLap = try resultContainer.decode(FastestLap.self, forKey: .fastestLap)
//    }
}

struct Time: Decodable {
    var millis: String?
    var time: String
    
    enum CodingKeys: String, CodingKey {
        case millis, time
    }
    
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//
//       // millis = try container.decode(String.self, forKey: .millis)
//        time = try container.decode(String.self, forKey: .time)
//    }
}

struct FastestLap: Decodable {
    var rank: String
    var lap: String
    var time: Time
    
    enum CodingKeys: String, CodingKey {
        case rank, lap
        case time = "Time"
    }
    
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//
//        rank = try container.decode(String.self, forKey: .rank)
//        lap = try container.decode(String.self, forKey: .lap)
//        time = try container.decode(Time.self, forKey: .time)
//    }
}

struct AverageSpeed: Decodable {
    var units: String
    var speed: String
    
    enum CodingKeys: String, CodingKey {
        case units, speed
    }
    
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//
//        units = try container.decode(String.self, forKey: .units)
//        speed = try container.decode(String.self, forKey: .speed)
//    }
}
