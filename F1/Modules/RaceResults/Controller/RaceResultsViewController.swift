//
//  RaceResultsViewController.swift
//  F1
//
//  Created by Michael Yu on 4/21/21.
//

import Foundation
import UIKit

class RaceResultsViewController: UIViewController {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var raceLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.allowsSelection = false
            tableView.isScrollEnabled = true
        }
    }
    var viewModel: RaceResultsViewModel?
    
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        if let vm = viewModel {
           vm.fetchData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }
    
    private func setUpViews() {
        if let vm = viewModel {
            titleLabel.text = vm.getRace().country.uppercased()
            raceLabel.text = vm.getRace().raceNameString
        }
    }
    
    @IBAction private func openCircuitViewController(_ sender: Any) {
        let circuitVC = CircuitViewController.storyboardViewController()
        if let vm = viewModel {
            circuitVC.viewModel = CircuitViewModel(circuit: vm.getRace().circuit)
        }
        circuitVC.tab = "Results"
        circuitVC.modalPresentationStyle = .overCurrentContext
        circuitVC.hidesBottomBarWhenPushed = false
        self.navigationController?.pushViewController(circuitVC, animated: false)
    }
}

extension RaceResultsViewController: UITableViewDelegate {
}

extension RaceResultsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel?.numberOfRowsIn() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultsTableViewCell", for: indexPath) as? ResultsTableViewCell else { return UITableViewCell() }
        if let result = self.viewModel?.getResult(at: indexPath.row) {
            cell.configure(from: result)
        }
        return cell
    }
}

extension RaceResultsViewController: RaceResultsViewModelDelegate {
    func reloadData() {
        self.tableView.reloadData()
    }
    
    func startAnimatingActivityIndicator() {
        self.activityIndicatorView.startAnimating()
    }
    
    func stopAnimatingActivityIndicator() {
        self.activityIndicatorView.stopAnimating()
    }
}
