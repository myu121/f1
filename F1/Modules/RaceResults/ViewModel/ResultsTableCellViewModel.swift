//
//  ResultsTableCellViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/21/21.
//

import Foundation

protocol ResultsTableCellViewModelProtocol {
    var familyName: String { get }
    var givenName: String { get }
    var time: String { get }
    var position: String { get }
    var grid: String { get }
    var status: String { get }
    var points: String { get }
    var finishTime: String { get }
    var constructorId: String { get }
    var code: String { get }
}

class ResultsTableCellViewModel: ResultsTableCellViewModelProtocol {
    var givenName: String {
        result.driver.givenName
    }
    
    var points: String {
        result.points
    }
    
    var familyName: String {
        result.driver.familyName
    }
    
    var time: String {
        if let time = result.time?.time {
            return time
        } else {
            return result.status
        }
    }
    
    var finishTime: String {
        result.time?.time ?? ""
    }
    
    var position: String {
        result.position
    }
    
    var grid: String {
        if result.grid == "0" {
            return "PL"
        }
        return result.grid
    }
    
    var status: String {
        result.status
    }
    
    var constructorId: String {
        result.constructor.constructorId
    }
    
    var code: String {
        result.driver.code
    }
    
    private var result: Results
    
    init(result: Results) {
        self.result = result
    }
}
