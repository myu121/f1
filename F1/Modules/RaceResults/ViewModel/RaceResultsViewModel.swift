//
//  RaceResultsViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/21/21.
//

import Foundation

protocol RaceResultsViewModelDelegate: AnyObject {
    func reloadData()
    func startAnimatingActivityIndicator()
    func stopAnimatingActivityIndicator()
}

class RaceResultsViewModel {
    private var race: RaceCellViewModelProtocol
    weak var delegate: RaceResultsViewModelDelegate?
    private var results: [ResultsTableCellViewModelProtocol]
    private let router = Router<F1Api>()
    
    init(delegate: RaceResultsViewModelDelegate, race: RaceCellViewModelProtocol) {
        self.delegate = delegate
        self.race = race
        print(race.circuit)
        results = []
    }
    
    func fetchData() {
        delegate?.startAnimatingActivityIndicator()
        guard let round = Int(race.round) else { return }
        router.request(.raceResults((year: 2021, round: round))) { [weak self] (results: Result<RaceResults, AppError>) in
            guard let self = self else { return }
            self.delegate?.stopAnimatingActivityIndicator()
            switch results {
            case .success(let data):
                if let resultsData = data.mrData.raceTable.races[0].results {
                    self.results = resultsData.compactMap {
                            ResultsTableCellViewModel(result: $0)
                    }
                }
                self.delegate?.reloadData()
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getRace() -> RaceCellViewModelProtocol {
        race
    }
    
    func numberOfRowsIn() -> Int {
        results.count
    }
    
    func getResult(at index: Int) -> ResultsTableCellViewModelProtocol {
        results[index]
    }
}
