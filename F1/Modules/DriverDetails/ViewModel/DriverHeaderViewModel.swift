//
//  DriverHeaderViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/20/21.

import Foundation
import UIKit

protocol DriverHeaderViewModelProtocol {
    var driverImage: UIImage { get }
    var givenName: String { get }
    var familyName: String { get }
    var permanentNumber: String { get }
    var constructor: String { get }
    var constructorId: String { get }
    var flagImage: UIImage { get }
    var code: String { get }
}

class DriverHeaderViewModel: DriverHeaderViewModelProtocol {
    
    private var driverStanding: DriverStandingsCellViewModelProtocol
    
    var driverImage: UIImage {
        UIImage(named: driverStanding.familyName.lowercased()) ?? UIImage()
    }
    
    var givenName: String {
        driverStanding.givenName
    }
    
    var familyName: String {
        driverStanding.familyName
    }
    
    var permanentNumber: String {
        driverStanding.permanentNumber
    }
    
    var constructor: String {
        driverStanding.constructorName
    }
    
    var constructorId: String {
        driverStanding.constructorId
    }
    
    var flagImage: UIImage {
        UIImage(named: driverStanding.nationality) ?? UIImage()
    }
    
    var code: String {
        driverStanding.code
    }
    
    init(driverStanding: DriverStandingsCellViewModelProtocol) {
        self.driverStanding = driverStanding
    }
}
