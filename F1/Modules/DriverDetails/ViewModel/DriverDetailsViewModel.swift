//
//  DriverDetailsViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/20/21.
//

import Foundation

class DriverDetailsViewModel {
    
    private var driver: DriverStandingsCellViewModelProtocol
    var headerViewModel: DriverHeaderViewModelProtocol
    
    init(driver: DriverStandingsCellViewModelProtocol) {
        self.driver = driver
        self.headerViewModel = DriverHeaderViewModel(driverStanding: driver)
    }
    
    func getDriver() -> DriverStandingsCellViewModelProtocol {
        driver
    }
}
