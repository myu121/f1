//
//  DriverDetailsViewController.swift
//  F1
//
//  Created by Michael Yu on 4/20/21.
//

import Foundation
import UIKit

class DriverDetailsViewController: UIViewController {
    @IBOutlet private weak var driverHeaderView: DriverHeaderView!
    @IBOutlet private weak var winsLabel: UILabel!
    @IBOutlet private weak var divider1View: UIView!
    @IBOutlet private weak var divider2View: UIView!
    @IBOutlet private weak var pointsLabel: UILabel!
    @IBOutlet private weak var gPsEnteredLabel: UILabel!
    @IBOutlet private weak var podiumsLabel: UILabel!

    var viewModel: DriverDetailsViewModel?
    var podiums: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        podiums = getPodiums()
        setUpViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setUpViews() {
        if let vm = viewModel {
            driverHeaderView.configure(configurator: vm.headerViewModel)
            winsLabel.text = vm.getDriver().wins
            divider1View.layer.backgroundColor = UIColor(named: vm.getDriver().constructorId).cgColor
            divider2View.layer.backgroundColor = UIColor(named: vm.getDriver().constructorId).cgColor
            pointsLabel.text = vm.getDriver().points
            gPsEnteredLabel.text = String(UpcomingRacesViewModel.currentRound - 1)
            podiumsLabel.text = String(podiums)
        }
    }
    
    func getPodiums() -> Int {
        podiumsDict[viewModel?.getDriver().code ?? ""] ?? 0
    }
}
