//
//  DriverHeaderView.swift
//  F1
//
//  Created by Michael Yu on 4/20/21.
//

import Foundation
import UIKit

class DriverHeaderView: UIView {
    @IBOutlet private weak var driverImage: UIImageView!
    @IBOutlet private weak var givenNameLabel: UILabel!
    @IBOutlet private weak var familyNameLabel: UILabel!
    @IBOutlet private weak var permanentNumberLabel: UILabel!
    @IBOutlet private weak var teamLabel: UILabel!
    @IBOutlet private weak var teamView: UIView!
    @IBOutlet private weak var flagImageView: UIImageView!
    
    func configure(configurator: DriverHeaderViewModelProtocol) {
        driverImage.image = configurator.driverImage
        givenNameLabel.text = configurator.givenName
        familyNameLabel.text = configurator.familyName
        permanentNumberLabel.text = configurator.permanentNumber
        teamLabel.text = configurator.constructor
        teamView.layer.backgroundColor = UIColor(named: configurator.constructorId).cgColor
        flagImageView.image = configurator.flagImage
    }
}
