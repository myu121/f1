//
//  ViewController.swift
//  F1
//
//  Created by Michael Yu on 4/5/21.
//

import UIKit

class UpcomingRaceCalendarViewController: UIViewController, AlertProtocol {
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.allowsSelection = true
        }
    }
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    lazy var viewModel = UpcomingRacesViewModel(delegate: self)

    // MARK: - Public Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = true
    }

    // MARK: - Private Methods
    private func showAlert(message: String) {
        showAlert(title: "Error", message: message, buttons: []) { _, _ in
        }
    }
    
    @IBAction private func openPastRaceCalendarViewController(_ sender: Any) {
        let pastRacesVC = PastRaceCalendarViewController.storyboardViewController()
        pastRacesVC.viewModel = PastRacesViewModel(delegate: pastRacesVC, pastRaces: self.viewModel.getPastRaces())
            
        pastRacesVC.modalPresentationStyle = .overCurrentContext
        pastRacesVC.hidesBottomBarWhenPushed = false
        self.navigationController?.pushViewController(pastRacesVC, animated: false)
    }
}

// MARK: - Extensions
extension UpcomingRaceCalendarViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = view.frame.size.height
        let width = view.frame.size.width
        return CGSize(width: width, height: height * 0.12)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedRace = viewModel.race(at: indexPath.row)
        let vc = RaceDetailsViewController.storyboardViewController()
        vc.viewModel = RaceDetailsViewModel(delegate: vc, race: selectedRace)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension UpcomingRaceCalendarViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.numberOfRowsIn(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RaceCalendarCollectionViewCell", for: indexPath) as? RaceCalendarCollectionViewCell else {
            fatalError("Unable to dequeue cell")
        }
        let race = viewModel.race(at: indexPath.row)
        cell.configure(configurator: race)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        CGSize(width: 100, height: 20)
    }
}

extension UpcomingRaceCalendarViewController: UICollectionViewDelegateFlowLayout {}

extension UpcomingRaceCalendarViewController: UpcomingRacesViewModelDelegate {
    func getPastRaces(pastRaces: [RaceCellViewModelProtocol]) {
    }
    
    func reloadData() {
        self.collectionView.reloadData()
    }
    
    func didFail(with error: AppError) {
        self.showAlert(title: "Error", message: error.errorMessage, buttons: [.ok]) { _, _ in
        }
    }
    
    func startAnimatingActivityIndicator() {
        self.activityIndicatorView.startAnimating()
    }
    
    func stopAnimatingActivityIndicator() {
        self.activityIndicatorView.stopAnimating()
    }
}
