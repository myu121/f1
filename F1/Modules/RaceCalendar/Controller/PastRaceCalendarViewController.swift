//
//  PastRaceCalendarViewController.swift
//  F1
//
//  Created by Michael Yu on 4/11/21.
//

import Foundation
import UIKit

class PastRaceCalendarViewController: UIViewController, AlertProtocol {
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.allowsSelection = true
            self.collectionView.reloadData()
        }
    }
    
    var viewModel: PastRacesViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    @IBAction private func openUpcomingRaceCalendarViewController(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

extension PastRaceCalendarViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = view.frame.size.height
        let width = view.frame.size.width
        return CGSize(width: width, height: height * 0.12)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = RaceResultsViewController.storyboardViewController()
        if let vm = viewModel {
            let selectedRace = vm.race(at: indexPath.row)
            let raceResultsVM = RaceResultsViewModel(delegate: viewController, race: selectedRace)
            viewController.viewModel = raceResultsVM
        }
        
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension PastRaceCalendarViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel?.numberOfRowsIn(section: section) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RaceCalendarCollectionViewCell", for: indexPath) as? RaceCalendarCollectionViewCell else {
            fatalError("Unable to dequeue cell")
        }
        if let race = viewModel?.race(at: indexPath.row) {
            cell.configure(configurator: race)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        CGSize(width: 100, height: 20)
    }
}

extension PastRaceCalendarViewController: UICollectionViewDelegateFlowLayout {}

extension PastRaceCalendarViewController: PastRacesViewModelDelegate {
    func reloadData() {
        self.collectionView.reloadData()
    }
    
    func didFail(with error: AppError) {
        self.showAlert(title: "Error", message: error.errorMessage, buttons: [.ok]) { _, _ in
        }
    }
}
