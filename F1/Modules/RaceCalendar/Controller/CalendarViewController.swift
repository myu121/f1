//
//  CalendarViewController.swift
//  F1
//
//  Created by Michael Yu on 4/13/21.
//

import Foundation
import UIKit

class CalendarViewController: UIViewController {

    @IBOutlet private weak var scrollView: UIScrollView!
    var calendars: [Calendar] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calendars = createCalendars()
        setupScrollView(pages: calendars)
    }
    
    func createCalendars() -> [Calendar] {

        guard let upcomingRaces: Calendar = Bundle.main.loadNibNamed("UpcomingRaces", owner: self, options: nil)?.first as? Calendar else { fatalError("failed to create xib") }
            
        guard let pastRaces: Calendar = Bundle.main.loadNibNamed("UpcomingRaces", owner: self, options: nil)?.first as? Calendar else { fatalError("failed to create xib") }
        
        return [upcomingRaces, pastRaces]
    }
    
    func setupScrollView(pages: [Calendar]) {
          scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
          scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(calendars.count), height: view.frame.height)
          scrollView.isPagingEnabled = true
          
          for index in 0 ..< calendars.count {
            calendars[index].frame = CGRect(x: view.frame.width * CGFloat(index), y: 0, width: view.frame.width, height: view.frame.height)
              scrollView.addSubview(calendars[index])
          }
      }
}
