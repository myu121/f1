//
//  Model.swift
//  F1
//
//  Created by Michael Yu on 4/21/21.
//

import Foundation

enum F1Api {
    case driverStandings(year: String)
    case constructorStandings(year: String)
    case raceResults((year: Int, round: Int))
    case raceCalendar(year: Int)
}

extension F1Api: EndPointType {
    var path: String {
        switch self {
        case .driverStandings(let year):
            return "\(year)/driverStandings.json"
        case .constructorStandings(let year):
            return "\(year)/constructorStandings.json"
        case .raceResults(let (year, round)):
            return "\(year)/\(round)/results.json"
        case .raceCalendar(let year):
            return "\(year).json"
        }
    }
}
