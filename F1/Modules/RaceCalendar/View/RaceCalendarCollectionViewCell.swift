//
//  RaceCalendarCollectionViewCell.swift
//  F1
//
//  Created by Michael Yu on 4/5/21.
//

import UIKit

class RaceCalendarCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var roundLabel: UILabel!
    @IBOutlet private weak var raceLabel: UILabel!
    @IBOutlet private weak var countryLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var monthLabel: UILabel!
    @IBOutlet private weak var cellBGView: UIView!
    @IBOutlet private weak var monthView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCellViews()
    }
    
    func configure(configurator: RaceCellViewModelProtocol) {
        roundLabel.text = configurator.roundString
        raceLabel.text = configurator.raceNameString
        countryLabel.text = configurator.country
        monthLabel.text = configurator.month
        dateLabel.text = configurator.dates
    }
    
    func configureCellViews() {
        cellBGView.layer.cornerRadius = 6
        monthView.layer.cornerRadius = 10
    }
}
