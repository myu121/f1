//
//  UpcomingRacesViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/17/21.
//

import Foundation

protocol UpcomingRacesViewModelDelegate: AnyObject {
    func reloadData()
    func didFail(with error: AppError)
    func startAnimatingActivityIndicator()
    func stopAnimatingActivityIndicator()
}

class UpcomingRacesViewModel {
    static var currentRound: Int = 0
    private var upcomingRaces: [RaceCellViewModelProtocol] {
        didSet {
           self.delegate?.reloadData()
        }
    }
    private var allRaces: [RaceCellViewModelProtocol]
    private var pastRaces: [RaceCellViewModelProtocol]
    weak var delegate: UpcomingRacesViewModelDelegate?
    private let router = Router<F1Api>()

    // MARK: - Initializers
    init(delegate: UpcomingRacesViewModelDelegate) {
        self.delegate = delegate
        self.upcomingRaces = []
        self.allRaces = []
        self.pastRaces = []
    }
    
    // MARK: - Public Methods
    func fetchData() {
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        delegate?.startAnimatingActivityIndicator()
        router.request(.raceCalendar(year: 2021)) { [weak self] (results: Result<RaceCalendar, AppError>) in
            self?.delegate?.stopAnimatingActivityIndicator()
            guard let self = self else { return }
            
            switch results {
            case .success(let data):
                self.allRaces = data.mrData.raceTable.races.compactMap {
                    RaceCellViewModel(race: $0)
                }
                self.splitRaceDataByDate()
                
            case .failure(let error):
                self.delegate?.didFail(with: error)
            }
            dispatchGroup.leave()
        }
        let utilityQueue = DispatchQueue(label: "utilityQueue", qos: .utility)
        dispatchGroup.notify(queue: utilityQueue, work: DispatchWorkItem { [weak self] in
            self?.countPodiums()
        })
    }
    
    func splitRaceDataByDate() {
        allRaces.forEach { race in
            let today = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let raceDate = formatter.date(from: race.date)
            if today.compare(raceDate ?? Date()).rawValue == 1 {
                self.pastRaces.append(race)
            } else {
                self.upcomingRaces.append(race)
            }
        }
        self.delegate?.reloadData()
        UpcomingRacesViewModel.currentRound = Int(upcomingRaces[0].round) ?? 0
    }
    
    func numberOfRowsIn(section: Int) -> Int {
        self.upcomingRaces.count
    }
    
    func race(at index: Int) -> RaceCellViewModelProtocol {
        self.upcomingRaces[index]
    }
    
    func getPastRaces() -> [RaceCellViewModelProtocol] {
        self.pastRaces
    }
    
    func countPodiums() {
        for race in pastRaces {
            guard let round = Int(race.round) else { return }
            router.request(.raceResults((year: 2021, round: round))) { [weak self] (results: Result<RaceResults, AppError>) in
                guard let self = self else { return }
                
                switch results {
                case .success(let data):
                    if let resultsData = data.mrData.raceTable.races[0].results {
                        for index in 0...2 {
                            if podiumsDict.keys.contains(resultsData[index].driver.code) {
                                if let count =
                                    podiumsDict[resultsData[index].driver.code] {
                                    podiumsDict[resultsData[index].driver.code] = count + 1
                                }
                            } else {
                                podiumsDict[resultsData[index].driver.code] = 1
                            }
                        }
                    }
                    
                case .failure(let error):
                    self.delegate?.didFail(with: error)
                }
            }
        }
    }
}
