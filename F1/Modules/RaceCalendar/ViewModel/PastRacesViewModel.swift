//
//  PastRacesViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/17/21.
//

import Foundation

protocol PastRacesViewModelDelegate: AnyObject {
    func reloadData()
    func didFail(with error: AppError)
}

class PastRacesViewModel {
    private var pastRaces: [RaceCellViewModelProtocol] = []
    weak var delegate: PastRacesViewModelDelegate?

    init(delegate: PastRacesViewModelDelegate, pastRaces: [RaceCellViewModelProtocol]) {
        self.delegate = delegate
        self.pastRaces = pastRaces
    }
    
    func numberOfRowsIn(section: Int) -> Int {
        pastRaces.count
    }
    
    func race(at index: Int) -> RaceCellViewModelProtocol {
        pastRaces[index]
    }
}
