//
//  RaceCellViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/17/21.
//

import Foundation

protocol RaceCellViewModelProtocol {
    var round: String { get }
    var raceName: String { get }
    var country: String { get }
    var date: String { get }
    var circuit: Circuit { get }
    var time: String { get }
    var month: String { get }
    var dates: String { get }
    var roundString: String { get }
    var raceNameString: String { get }
}

class RaceCellViewModel: RaceCellViewModelProtocol {
    
    private var race: Race
    
    init(race: Race) {
        self.race = race
    }
    
    var round: String {
        race.round
    }
    
    var roundString: String {
        "Round \(race.round)"
    }
    
    var raceName: String {
        race.raceName
    }
    
    var raceNameString: String {
        "Formula 1 \(race.raceName) 2021"
    }
    
    var country: String {
        race.circuit.location.country
    }
    
    var date: String {
        race.date
    }
    
    var dates: String {
        var date = calendarDict[race.round]?[0]
        date?.append("-")
        date?.append(calendarDict[race.round]?[6] ?? "")
        return date ?? ""
    }
    
    var circuit: Circuit {
        race.circuit
    }
    
    var time: String {
        race.time
    }
    
    var month: String {
        calendarDict[race.round]?[8] ?? ""
    }
}
