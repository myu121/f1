//
//  RaceCalendarModel.swift
//  F1
//
//  Created by Michael Yu on 4/5/21.
//

import Foundation

struct RaceCalendar: Decodable {
    var mrData: MRDataCalendar
    
    enum CodingKeys: String, CodingKey {
        case mrData = "MRData"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        mrData = try container.decode(MRDataCalendar.self, forKey: .mrData)
    }
}

struct MRDataCalendar: Decodable {
    var series: String
    var url: String
    var limit: String
    var offset: String
    var total: String
    var raceTable: RaceTable

    enum CodingKeys: String, CodingKey {
        case series, url, limit, offset, total
        case raceTable = "RaceTable"
    }
}

struct RaceTable: Decodable {
    var season: String
    var round: String?
    var races: [Race]
    
    enum CodingKeys: String, CodingKey {
        case season, round
        case races = "Races"
    }
}

struct Race: Decodable {
    var season: String
    var round: String
    var url: String
    var raceName: String
    var circuit: Circuit
    var date: String
    var time: String
    var results: [Results]?
    
    enum CodingKeys: String, CodingKey {
        case season, round, url, date, time, raceName
        case circuit = "Circuit"
        case results = "Results"
    }
}

struct Circuit: Decodable {
    var circuitId: String
    var url: String
    var circuitName: String
    var location: Location
    
    enum CodingKeys: String, CodingKey {
        case circuitId, url, circuitName
        case location = "Location"
    }
}

struct Location: Decodable {
    var lat: String
    var long: String
    var locality: String
    var country: String
}
