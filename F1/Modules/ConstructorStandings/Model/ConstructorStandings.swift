//
//  ConstructorStandingsModel.swift
//  F1
//
//  Created by Michael Yu on 4/6/21.
//

import Foundation

struct ConstructorStandings: Decodable {
    var mrData: MRDataConstructorStandings
    
    enum CodingKeys: String, CodingKey {
        case mrData = "MRData"
    }
}

struct MRDataConstructorStandings: Decodable {
    var xmlns: String
    var series: String
    var url: String
    var limit: String
    var offset: String
    var total: String
    var standingsTable: ConstructorStandingsTable
    
    enum CodingKeys: String, CodingKey {
        case xmlns, series, url, limit, offset, total
        case standingsTable = "StandingsTable"
    }
}

struct ConstructorStandingsTable: Decodable {
    var season: String
    var standingsLists: [ConstructorStandingsList]
    
    enum CodingKeys: String, CodingKey {
        case season
        case standingsLists = "StandingsLists"
    }
}

struct ConstructorStandingsList: Decodable {
    var season: String
    var round: String
    var constructorStandings: [ConstructorStanding]
    
    enum CodingKeys: String, CodingKey {
        case season, round
        case constructorStandings = "ConstructorStandings"
    }
}

struct ConstructorStanding: Decodable {
    var position: String
    var positionText: String
    var points: String
    var wins: String
    var constructor: Constructor
    
    enum CodingKeys: String, CodingKey {
        case position, positionText, points, wins
        case constructor = "Constructor"
    }
}
