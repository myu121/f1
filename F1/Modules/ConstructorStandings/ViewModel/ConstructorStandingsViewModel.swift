//
//  ConstructorStandingsViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/17/21.
//

import Foundation
import UIKit

protocol ConstructorStandingsViewModelDelegate: AnyObject {
    func reloadData()
    func didFail(with error: AppError)
    func startAnimatingActivityIndicator()
    func stopAnimatingActivityIndicator()
}

class ConstructorStandingsViewModel {
    private var standingsData: [ConstructorStandingsCellViewModelProtocol] {
        didSet {
            self.delegate?.reloadData()
        }
    }
    
    weak var delegate: ConstructorStandingsViewModelDelegate?
    private let router = Router<F1Api>()

    init(delegate: ConstructorStandingsViewModelDelegate) {
        self.delegate = delegate
        self.standingsData = []
    }
    
    func fetchData() {
        delegate?.startAnimatingActivityIndicator()
        router.request(.constructorStandings(year: "2021")) { [weak self] (results: Result<ConstructorStandings, AppError>) in
            self?.delegate?.stopAnimatingActivityIndicator()
            guard let self = self else { return }
            
            switch results {
            case .success(let data):
                self.standingsData = data.mrData.standingsTable.standingsLists[0].constructorStandings.compactMap {
                    ConstructorStandingsCellViewModel(constructor: $0)
                }
                self.delegate?.reloadData()
                
            case .failure(let error):
                self.delegate?.didFail(with: error)
            }
        }
    }
    
    func numberOfRowsIn(section: Int) -> Int {
        self.standingsData.count
    }
    
    func constructor(at index: Int) -> ConstructorStandingsCellViewModelProtocol {
        self.standingsData[index]
    }
}
