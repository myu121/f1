//
//  ConstructorStandingsCellViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/17/21.

import Foundation

protocol ConstructorStandingsCellViewModelProtocol {
    var constructor: Constructor { get }
    var position: String { get }
    var points: String { get }
}

class ConstructorStandingsCellViewModel: ConstructorStandingsCellViewModelProtocol {
    private var constructorStanding: ConstructorStanding
    
    init(constructor: ConstructorStanding) {
        constructorStanding = constructor
    }
    
    var constructor: Constructor {
        constructorStanding.constructor
    }
    
    var position: String {
        constructorStanding.position
    }
    
    var points: String {
        constructorStanding.points
    }
}
