//
//  ConstructorStandingsCollectionViewCell.swift
//  F1
//
//  Created by Michael Yu on 4/6/21.
//

import UIKit

class ConstructorStandingsCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var constructorNameLabel: UILabel!
    @IBOutlet private weak var positionLabel: UILabel!
    @IBOutlet private weak var pointsLabel: UILabel!
    @IBOutlet private weak var driversLabel: UILabel!
    @IBOutlet private weak var cellBGView: UIView!
    @IBOutlet private weak var pointsView: UIView!
    @IBOutlet private weak var dividerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCellViews()
    }
    
    func configure(configurator: ConstructorStandingsCellViewModelProtocol) {
        constructorNameLabel.text = configurator.constructor.name
        positionLabel.text = configurator.position
        pointsLabel.text = configurator.points
        dividerView.layer.backgroundColor = UIColor(named: configurator.constructor.constructorId).cgColor
        driversLabel.text = driverData[configurator.constructor.constructorId]
    }
    
    func configureCellViews() {
        cellBGView.layer.cornerRadius = 6
        pointsView.layer.cornerRadius = 12
    }
}
