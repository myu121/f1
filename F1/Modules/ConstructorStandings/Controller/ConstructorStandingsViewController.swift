//
//  ConstructorStandingsViewController.swift
//  F1
//
//  Created by Michael Yu on 4/6/21.
//

import Foundation
import UIKit

class ConstructorStandingsViewController: UIViewController, AlertProtocol {
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.reloadData()
        }
    }
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    lazy var viewModel = ConstructorStandingsViewModel(delegate: self)

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = true
    }
    
    private func showAlert(message: String) {
        showAlert(title: "Error", message: message, buttons: []) { _, _ in
        }
    }
    
    @IBAction private func openDriversStandingsViewController(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}

extension ConstructorStandingsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = view.frame.size.height
        let width = view.frame.size.width
        return CGSize(width: width, height: height * 0.1)
    }
}

extension ConstructorStandingsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.numberOfRowsIn(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConstructorStandingsCollectionViewCell", for: indexPath) as? ConstructorStandingsCollectionViewCell else {
            fatalError("Unable to dequeue cell")
        }
        let constructor = viewModel.constructor(at: indexPath.row)
        cell.configure(configurator: constructor)
        return cell
    }
}

extension ConstructorStandingsViewController: UICollectionViewDelegateFlowLayout {}

extension ConstructorStandingsViewController: ConstructorStandingsViewModelDelegate {
    func reloadData() {
        self.collectionView.reloadData()
    }
    
    func didFail(with error: AppError) {
        self.showAlert(title: "Error", message: error.errorMessage, buttons: [.ok]) { _, _ in
        }
    }
    
    func startAnimatingActivityIndicator() {
        self.activityIndicatorView.startAnimating()
    }
    
    func stopAnimatingActivityIndicator() {
        self.activityIndicatorView.stopAnimating()
    }
}
