//
//  DriverStandingsViewController.swift
//  F1
//
//  Created by Michael Yu on 4/5/21.
//

import Foundation
import UIKit

class DriverStandingsViewController: UIViewController, AlertProtocol {
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    lazy var viewModel = DriverStandingsViewModel(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = true
    }
    
    private func showAlert(message: String) {
        showAlert(title: "Error", message: message, buttons: []) { _, _ in
        }
    }
    
    @IBAction private func openConstructorsStandingsVC(_ sender: Any) {
        let constructorsVC = ConstructorStandingsViewController.storyboardViewController()
        constructorsVC.modalPresentationStyle = .overCurrentContext
        constructorsVC.hidesBottomBarWhenPushed = false
        self.navigationController?.pushViewController(constructorsVC, animated: false)
    }
}

extension DriverStandingsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = view.frame.size.height
        let width = view.frame.size.width
        return CGSize(width: width, height: height * 0.08)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let driver = viewModel.driver(at: indexPath.row)
        let vc = DriverDetailsViewController.storyboardViewController()
        vc.viewModel = DriverDetailsViewModel(driver: driver)
            navigationController?.pushViewController(vc, animated: true)
    }
}

extension DriverStandingsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.numberOfRowsIn(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DriverStandingsCollectionViewCell", for: indexPath) as? DriverStandingsCollectionViewCell else {
            fatalError("Unable to dequeue cell")
        }
        let driver = viewModel.driver(at: indexPath.row)
        cell.configure(configurator: driver)
        return cell
    }
}

extension DriverStandingsViewController: UICollectionViewDelegateFlowLayout {}

extension DriverStandingsViewController: DriverStandingsViewModelDelegate {
    func reloadData() {
        self.collectionView.reloadData()
    }
    
    func didFail(with error: AppError) {
        self.showAlert(title: "Error", message: error.errorMessage, buttons: [.ok]) { _, _ in
        }
    }
    
    func startAnimatingActivityIndicator() {
        self.activityIndicatorView.startAnimating()
    }
    
    func stopAnimatingActivityIndicator() {
        self.activityIndicatorView.stopAnimating()
    }
}
