//
//  DriverStandingsCellViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/17/21.

import Foundation

protocol DriverStandingsCellViewModelProtocol {
    var constructorName: String { get }
    var constructorId: String { get }
    var givenName: String { get }
    var familyName: String { get }
    var position: String { get }
    var points: String { get }
    var permanentNumber: String { get }
    var nationality: String { get }
    var wins: String { get }
    var code: String { get }
}

class DriverStandingsCellViewModel: DriverStandingsCellViewModelProtocol {
    
    private var driverStanding: DriverStanding
    
    init(driverStanding: DriverStanding) {
        self.driverStanding = driverStanding
    }
    
    var constructorName: String {
        driverStanding.constructors[0].name
    }
    
    var position: String {
        driverStanding.position
    }
    
    var points: String {
        driverStanding.points
    }
    
    var constructorId: String {
        driverStanding.constructors[0].constructorId
    }
    
    var givenName: String {
        driverStanding.driver.givenName
    }
    
    var familyName: String {
        driverStanding.driver.familyName
    }
    
    var permanentNumber: String {
        driverStanding.driver.permanentNumber ?? ""
    }
    
    var nationality: String {
        driverStanding.driver.nationality
    }
    
    var wins: String {
        driverStanding.wins
    }
    
    var code: String {
        driverStanding.driver.code
    }
}
