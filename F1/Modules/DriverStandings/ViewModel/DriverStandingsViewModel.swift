//
//  DriverStandingsViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/17/21.
//

import Foundation
import UIKit

protocol DriverStandingsViewModelDelegate: AnyObject {
    func reloadData()
    func didFail(with error: AppError)
    func startAnimatingActivityIndicator()
    func stopAnimatingActivityIndicator()
}

class DriverStandingsViewModel {
    private var standingsData: [DriverStandingsCellViewModelProtocol] {
        didSet {
            self.delegate?.reloadData()
        }
    }
    
    weak var delegate: DriverStandingsViewModelDelegate?
    private let router = Router<F1Api>()

    init(delegate: DriverStandingsViewModelDelegate) {
        self.delegate = delegate
        self.standingsData = []
    }
    
    func fetchData() {
        delegate?.startAnimatingActivityIndicator()
        router.request(.driverStandings(year: "2021")) { [weak self] (results: Result<DriverStandings, AppError>) in
            self?.delegate?.stopAnimatingActivityIndicator()
            guard let self = self else { return }
            
            switch results {
            case .success(let data):
                self.standingsData = data.mrData.standingsTable.standingsLists[0].driverStandings.compactMap {
                    DriverStandingsCellViewModel(driverStanding: $0)
                }
                self.delegate?.reloadData()
                
            case .failure(let error):
                self.delegate?.didFail(with: error)
            }
        }
    }
    
    func numberOfRowsIn(section: Int) -> Int {
        self.standingsData.count
    }
    
    func driver(at index: Int) -> DriverStandingsCellViewModelProtocol {
        self.standingsData[index]
    }
}
