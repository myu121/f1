//
//  DriverStandings.swift
//  F1
//
//  Created by Michael Yu on 4/5/21.
//

import Foundation

struct DriverStandings: Decodable {
    var mrData: MRDataStandings
    
    enum CodingKeys: String, CodingKey {
        case mrData = "MRData"
    }
}

struct MRDataStandings: Decodable {
    var xmlns: String
    var series: String
    var url: String
    var limit: String
    var offset: String
    var total: String
    var standingsTable: DriverStandingsTable
    
    enum CodingKeys: String, CodingKey {
        case xmlns, series, url, limit, offset, total
        case standingsTable = "StandingsTable"
    }
}

struct DriverStandingsTable: Decodable {
    var season: String
    var standingsLists: [DriverStandingsList]
    
    enum CodingKeys: String, CodingKey {
        case season
        case standingsLists = "StandingsLists"
    }
}

struct DriverStandingsList: Decodable {
    var season: String
    var round: String
    var driverStandings: [DriverStanding]
    
    enum CodingKeys: String, CodingKey {
        case season, round
        case driverStandings = "DriverStandings"
    }
}

struct DriverStanding: Decodable {
    var position: String
    var positionText: String
    var points: String
    var wins: String
    var driver: Driver
    var constructors: [Constructor]
    
    enum CodingKeys: String, CodingKey {
        case position, positionText, points, wins
        case driver = "Driver"
        case constructors = "Constructors"
    }
}

struct Driver: Decodable {
    var driverId: String
    var permanentNumber: String?
    var code: String
    var url: String
    var givenName: String
    var familyName: String
    var dateOfBirth: String
    var nationality: String
}

struct Constructor: Decodable {
    var constructorId: String
    var url: String
    var name: String
    var nationality: String
}
