//
//  DriverStandingsCollectionViewCell.swift
//  F1
//
//  Created by Michael Yu on 4/5/21.
//

import UIKit

class DriverStandingsCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var givenNameLabel: UILabel!
    @IBOutlet private weak var familyNameLabel: UILabel!
    @IBOutlet private weak var positionLabel: UILabel!
    @IBOutlet private weak var constructorLabel: UILabel!
    @IBOutlet private weak var pointsLabel: UILabel!
    @IBOutlet private weak var cellBGView: UIView!
    @IBOutlet private weak var pointsView: UIView!
    @IBOutlet private weak var dividerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCellViews()
    }
    
    func configure(configurator: DriverStandingsCellViewModelProtocol) {
        givenNameLabel.text = configurator.givenName
        familyNameLabel.text = configurator.familyName
        positionLabel.text = configurator.position
        constructorLabel.text = configurator.constructorName
        pointsLabel.text = configurator.points
        dividerView.layer.backgroundColor = UIColor(named: configurator.constructorId).cgColor
    }
    
    func configureCellViews() {
        cellBGView.layer.cornerRadius = 6
        pointsView.layer.cornerRadius = 12
    }
}
