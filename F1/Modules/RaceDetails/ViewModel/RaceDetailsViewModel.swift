//
//  RaceDetailsViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/15/21.
//

import Foundation
import UIKit

protocol RaceDetailsViewModelDelegate: AnyObject {
}

class RaceDetailsViewModel {
    private var race: RaceCellViewModelProtocol
    private var sessions: [ScheduleTableCellViewModelProtocol]
    weak var delegate: RaceDetailsViewModelDelegate?
    var countdownViewModel: CountdownViewModelProtocol?
    
    init(delegate: RaceDetailsViewModelDelegate, race: RaceCellViewModelProtocol) {
        self.race = race
        self.delegate = delegate
        self.sessions = []
        self.populateSessions()
        self.setUpCountdownViewModel()
    }
    
    private func setUpCountdownViewModel() {
        self.countdownViewModel = CountdownViewModel(countdown: Countdown(date: race.date, round: race.round, location: race.circuit.location, circuit: race.circuit, time: race.time))
    }
    
    private func populateSessions() {
        var monthIndex: Int = 8
        if calendarDict[race.round]?[8].count ?? 0 > 3 {
            monthIndex = 10
        }
        sessions.append(ScheduleTableViewModel(session: Session(date: calendarDict[race.round]?[6] ?? "",
                                                                time: calendarDict[race.round]?[7] ?? "",
                                                                session: "Race",
                                                                month: calendarDict[race.round]?[monthIndex] ?? "")))
        
        if calendarDict[race.round]?[3] ?? "" == "01" {
            monthIndex = 9
        }
        sessions.append(ScheduleTableViewModel(session: Session(date: calendarDict[race.round]?[3] ?? "",
                                                                time: calendarDict[race.round]?[5] ?? "",
                                                                session: "Qualifying",
                                                                month: calendarDict[race.round]?[monthIndex] ?? "")))
        sessions.append(ScheduleTableViewModel(session: Session(date: calendarDict[race.round]?[3] ?? "",
                                                                time: calendarDict[race.round]?[4] ?? "",
                                                                session: "Practice 3",
                                                                month: calendarDict[race.round]?[monthIndex] ?? "")))
        
        sessions.append(ScheduleTableViewModel(session: Session(date: calendarDict[race.round]?[0] ?? "",
                                                                time: calendarDict[race.round]?[2] ?? "",
                                                                session: "Practice 2",
                                                                month: calendarDict[race.round]?[monthIndex] ?? "")))
        sessions.append(ScheduleTableViewModel(session: Session(date: calendarDict[race.round]?[0] ?? "",
                                                                time: calendarDict[race.round]?[1] ?? "",
                                                                session: "Practice 1",
                                                                month: calendarDict[race.round]?[monthIndex] ?? "")))
    }
    
    func getRace() -> RaceCellViewModelProtocol {
        race
    }
    
    func numberOfRowsIn() -> Int {
        self.sessions.count
    }
    
    func session(at index: Int) -> ScheduleTableCellViewModelProtocol {
        self.sessions[index]
    }
}
