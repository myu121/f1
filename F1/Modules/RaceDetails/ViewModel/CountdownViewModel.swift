//
//  CountdownViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/16/21.
//

import Foundation
import UIKit

protocol CountdownViewModelProtocol {
    var location: Location { get }
    var date: String { get }
    var round: String { get }
    var circuit: Circuit { get }
    var time: String { get }
    var minutes: String { get }
    var hours: String { get }
    var days: String { get }
    var dates: String { get }
    var country: String { get }
    var month: String { get }
    var trackImage: UIImage { get }
}

class CountdownViewModel: CountdownViewModelProtocol {
    var location: Location {
        self.countdown.location
    }
    
    var country: String {
        countdown.location.country.uppercased()
    }
    
    var date: String {
        self.countdown.date
    }
    
    var round: String {
        self.countdown.round
    }
    
    var circuit: Circuit {
        self.countdown.circuit
    }
    
    var time: String {
        self.countdown.time
    }
    
    var minutes: String {
        addLeadingZero(getMinutes() % 60)
    }
    
    var hours: String {
        addLeadingZero(getHours() % 24)
    }
    
    var days: String {
        addLeadingZero(getHours() / 24)
    }
    
    var month: String {
        calendarDict[countdown.round]?[8] ?? ""
    }
    
    var dates: String {
        print(countdown.round)
        var date = calendarDict[countdown.round]?[0]
        date?.append("-")
        date?.append(calendarDict[countdown.round]?[6] ?? "")
        return date ?? ""
    }
    
    var trackImage: UIImage {
        let track = countdown.circuit.circuitId
        return UIImage(named: "\(track)_small" ) ?? UIImage()
    }
    
    private var countdown: Countdown
    private var formattedDate: Date?
    
    init(countdown: Countdown) {
        self.countdown = countdown
        setUpCountdownDate()
    }
    
    private func setUpCountdownDate() {
        let raceDateString = countdown.date + " " + countdown.time
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
        formattedDate = dateFormatter.date(from: raceDateString) ?? Date()
    }
    
    private func getMinutes() -> Int {
        if let formattedDate = formattedDate {
            let seconds = Date().distance(to: formattedDate)
            let minutes = Int(seconds / 60)
            return minutes
        }
        return 0
    }
    
    private func getHours() -> Int {
        getMinutes() / 60
    }
}
