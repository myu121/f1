//
//  CircuitViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/15/21.
//

import Foundation

class CircuitViewModel {
    private var circuit: Circuit
    
    init(circuit: Circuit) {
        self.circuit = circuit
    }
    
    func getCircuit() -> Circuit {
        circuit
    }
}
