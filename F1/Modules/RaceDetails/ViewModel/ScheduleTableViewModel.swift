//
//  ScheduleTableViewModel.swift
//  F1
//
//  Created by Michael Yu on 4/15/21.
//

import Foundation

protocol ScheduleTableCellViewModelProtocol {
    var date: String { get }
    var month: String { get }
    var sessionString: String { get }
    var time: String { get }
}

class ScheduleTableViewModel: ScheduleTableCellViewModelProtocol {
    var date: String {
        self.session.date
    }
    
    var month: String {
        self.session.month
    }
    
    var sessionString: String {
        self.session.session
    }
    
    var time: String {
        self.session.time
    }
    
    private var session: Session
    
    init(session: Session) {
        self.session = session
    }
}
