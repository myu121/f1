//
//  ScheduleTableViewCell.swift
//  F1
//
//  Created by Michael Yu on 4/15/21.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {
    @IBOutlet private weak var monthContainerView: UIView!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var monthLabel: UILabel!
    @IBOutlet private weak var sessionLabel: UILabel!
    @IBOutlet private weak var timeLabel: UILabel!
    
    func configure(from configurator: ScheduleTableCellViewModelProtocol) {
        dateLabel.text = configurator.date
        monthLabel.text = configurator.month
        sessionLabel.text = configurator.sessionString
        timeLabel.text = configurator.time
        monthContainerView.layer.cornerRadius = 10
    }
}
