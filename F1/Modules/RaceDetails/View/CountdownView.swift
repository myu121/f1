//
//  BackgroundImageView.swift
//  F1
//
//  Created by Michael Yu on 4/16/21.
//

import Foundation
import UIKit

class CountdownView: UIView {
    @IBOutlet private weak var locationLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var monthLabel: UILabel!
    @IBOutlet private weak var trackImageView: UIImageView!
    @IBOutlet private weak var countdownSubView: UIView!
    @IBOutlet private weak var gpView: UIView!
    @IBOutlet private weak var daysLabel: UILabel!
    @IBOutlet private weak var hoursLabel: UILabel!
    @IBOutlet private weak var minutesLabel: UILabel!
    
    func configure(from configurator: CountdownViewModelProtocol) {
        locationLabel.text = configurator.country
        dateLabel.text = configurator.dates
        monthLabel.text = configurator.month
        trackImageView.image = configurator.trackImage
        gpView.layer.cornerRadius = 10
        countdownSubView.layer.cornerRadius = 10
        minutesLabel.text = configurator.minutes
        hoursLabel.text = configurator.minutes
        daysLabel.text = configurator.days
    }
}
