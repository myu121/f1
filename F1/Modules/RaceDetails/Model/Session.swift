//
//  Session.swift
//  F1
//
//  Created by Michael Yu on 4/15/21.
//

import Foundation

struct Session {
    var date: String
    var time: String
    var session: String
    var month: String
}
