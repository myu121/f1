//
//  Countdown.swift
//  F1
//
//  Created by Michael Yu on 4/16/21.
//

import Foundation

struct Countdown {
    var date: String
    var round: String
    var location: Location
    var circuit: Circuit
    var time: String
}
