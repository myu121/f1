//
//  CircuitViewController.swift
//  F1
//
//  Created by Michael Yu on 4/15/21.
//

import Foundation
import UIKit

class CircuitViewController: UIViewController {
    @IBOutlet private weak var flagImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var circuitLabel: UILabel!
    @IBOutlet private weak var circuitImageView: UIImageView!
    @IBOutlet private weak var countryLabel: UILabel!
    @IBOutlet private weak var lengthLabel: UILabel!
    @IBOutlet private weak var lapsLabel: UILabel!
    @IBOutlet private weak var distanceLabel: UILabel!
    @IBOutlet private weak var firstGPLabel: UILabel!
    @IBOutlet private weak var lapRecordLabel: UILabel!
    @IBOutlet private weak var lapRecordHolderLabel: UILabel!
    @IBOutlet private weak var tabButton: UIButton!

    var viewModel: CircuitViewModel?
    var tab: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let tab = tab {
            tabButton.setTitle(tab, for: .normal)
        }
    }
    
    private func setUpViews() {
        circuitImageView.image = UIImage(named: viewModel?.getCircuit().circuitId ?? "")
        titleLabel.textColor = .white
        titleLabel.text = viewModel?.getCircuit().location.country.uppercased()
        countryLabel.text = viewModel?.getCircuit().location.country.uppercased()
        circuitLabel.text = viewModel?.getCircuit().circuitName
        if var circuitString = viewModel?.getCircuit().location.country {
            circuitString.append("_flag")
            flagImageView.image = UIImage(named: circuitString)
            flagImageView.layer.cornerRadius = 10
            flagImageView.layer.masksToBounds = true
        }
        lengthLabel.text = circuitData[viewModel?.getCircuit().circuitId ?? ""]?[0]
        lapsLabel.text = circuitData[viewModel?.getCircuit().circuitId ?? ""]?[1]
        firstGPLabel.text = circuitData[viewModel?.getCircuit().circuitId ?? ""]?[2]
        distanceLabel.text = circuitData[viewModel?.getCircuit().circuitId ?? ""]?[3]
        lapRecordLabel.text = circuitData[viewModel?.getCircuit().circuitId ?? ""]?[4]
        lapRecordHolderLabel.text = circuitData[viewModel?.getCircuit().circuitId ?? ""]?[5]
    }
    
    @IBAction private func openScheduleViewController(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
}
