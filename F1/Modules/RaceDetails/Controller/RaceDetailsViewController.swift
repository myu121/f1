//
//  RaceDetailsViewController.swift
//  F1
//
//  Created by Michael Yu on 4/9/21.
//

import Foundation
import UIKit

class RaceDetailsViewController: UIViewController {
    @IBOutlet private weak var countdownView: CountdownView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var redBorderView: UIView!
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.allowsSelection = false
            tableView.isScrollEnabled = false
        }
    }
    var viewModel: RaceDetailsViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }
    
    private func setUpViews() {
        titleLabel.textColor = .white
        titleLabel.text = viewModel?.getRace().circuit.location.country.uppercased()
        if let description = viewModel?.getRace().raceName {
            descriptionLabel.text = "Formula 1 " + description + " 2021"
        }
        redBorderView.layer.cornerRadius = 15
        if let countdownVM = viewModel?.countdownViewModel {
            countdownView.configure(from: countdownVM)
        }
    }
    
    @IBAction private func openCircuitViewController(_ sender: Any) {
        let circuitVC = CircuitViewController.storyboardViewController()
        if let circuit = viewModel?.getRace().circuit {
            circuitVC.viewModel = CircuitViewModel(circuit: circuit)
        }
        circuitVC.modalPresentationStyle = .overCurrentContext
        circuitVC.hidesBottomBarWhenPushed = false
        self.navigationController?.pushViewController(circuitVC, animated: false)
    }
}

extension RaceDetailsViewController: UITableViewDelegate {
}

extension RaceDetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel?.numberOfRowsIn() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleTableViewCell", for: indexPath) as? ScheduleTableViewCell else { return UITableViewCell() }
        if let session = self.viewModel?.session(at: indexPath.row) {
            cell.configure(from: session)
        }
        return cell
    }
}

extension RaceDetailsViewController: RaceDetailsViewModelDelegate {
}
