//
//  UIColor.swift
//  F1
//
//  Created by Michael Yu on 4/7/21.
//

import Foundation
import UIKit

extension UIColor {
  enum ColorName: String {
    case mercedes
    case ferrari
    case redBull = "red_bull"
    case mclaren
    case alphatauri
    case haas
    case astonMartin = "aston_martin"
    case williams
    case alpine
    case alfa
    case f1
    
    var hexValue: UInt32 {
        switch self {
        case .mercedes:
            return 0x00d2beff
            
        case .ferrari:
            return 0xdc0000ff
            
        case .redBull:
            return 0x0600efff
            
        case .mclaren:
            return 0xff8700ff
            
        case .alphatauri:
            return 0x2b4562ff
            
        case .haas:
            return 0x000000ff
            
        case .astonMartin:
            return 0x006f62ff
            
        case .williams:
            return 0x005affff
            
        case .alpine:
            return 0x0090ffff
            
        case .alfa:
            return 0x00d2beff
            
        case .f1:
            return 0x00d2beff
        }
    }
  }
    
    convenience init(named name: String) {
        guard let color = ColorName(rawValue: name) else {
            self.init(red: 0, green: 0, blue: 0, alpha: 0)
            return
        }
        let rgbaValue = color.hexValue
        let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
        let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
        let blue  = CGFloat((rgbaValue >> 8) & 0xff) / 255.0
        let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
