# Formula 1 iOS App

This is an iOS application to display various Formula 1 related data, statistics, and information. Most of the data is taken from the [Ergast F1 API](http://ergast.com/mrd/).

Current functionality includes:

- 2021 Race Calendar separated by upcoming and past races
- Upcoming race dates, schedule, and countdown
- Past race results
- Driver standings
- Driver details
- Constructor standings
- Circuit information

<p align="center">
<img src="https://imgur.com/lwUk8I2.png">  <img src="https://imgur.com/aMYmruR.png">  <img src="https://imgur.com/OxC0h9Q.png">  <img src="https://imgur.com/4sjiyfu.png">  <img src="https://imgur.com/g5TY0wl.png">  <img src="https://imgur.com/W4i8Sqj.png">  <img src="https://imgur.com/M7uMcPs.png">
</p>
## Calendar
The calendar screen gets the current season from the API and displays it based on the current date, splitting the races into upcoming
and past races. The dates of each grand prix are listed on each cell.

Tapping on an upcoming race will show the schedule and a countdown until the race begins.

<img src="https://media.giphy.com/media/JBqNzvvqXOB2Vhr3OB/giphy.gif">

## Race Results
Tapping on a past race will go to the race results screen. This screen shows the race finish order with timings, points,
starting grid order, and status if a racer retired. All data is retrieved from the API.

<img src="https://media.giphy.com/media/3wiUUMGrgleyDQBdfj/giphy.gif">

## Driver and Constructor Standings
Driver and constructor standings with their respective points are listed in these screens. 
All data is retrieved from the API.

<img src="https://media.giphy.com/media/SiaU2Ikiqyv3olxIKc/giphy.gif">

## Driver Details
The driver detail screen lists the driver's championship standing, points, podiums, wins, and grand prixs entered in the current season.
Wins and points are taken from the API. Podiums are calculated based on top 3 finishes of the current season.

<img src="https://media.giphy.com/media/GHAgVI0bRfeq6KMC4Z/giphy.gif">

## Techniques

- JSON API fetching and parsing
- UIKit
- Collection View
- Table View
- Model-View-ViewModel (MVVM) code structure
- Delegates
- Observers

## API Reference

http://ergast.com/mrd/

## Technologies

- iOS 13.0 or above

- Xcode 12.5

- Swift 5

## Frameworks

- UIKit

## Supported Devices

- iPhone SE (2nd gen)

- iPhone 8 - 12 (All sizes supported)

## Legal
The F1 logos, F1 FORMULA 1 logos, F1 FIA FORMULA 1 WORLD CHAMPIONSHIP logo, FORMULA 1, FORMULA ONE, F1, FIA FORMULA ONE WORLD CHAMPIONSHIP, GRAND PRIX, F1 GRAND PRIX, FORMULA 1 GRAND PRIX and related marks are trademarks of Formula One Licensing BV, a Formula 1 company. All rights reserved.